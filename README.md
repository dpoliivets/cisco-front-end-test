# Cisco Front-End Test
by Dmytro Poliyivets.


### Quick Note
> There was a typo in the API in the task description. I'm not sure
> if it was intentional or not, but want to highlight just in case.
>
> What description states:
> http://api.tvmaze.com/search/shows?q=doctor%20who&embed=episodes
>
> What it actually should be:
> http://api.tvmaze.com/singlesearch/shows?q=doctor%20who&embed=episodes
> 
> The difference is /singlesearch/ vs /search/ routes.
> /search/ route cannot embed episodes in the response.


### Preview
If you don't want to download the repo and install the dependencies, you can preview the result on Netlify [cisco-front-end-test.netlify.com](https://cisco-front-end-test.netlify.com/)

### Installation

You’ll need to have Node 8.16.0 or Node 10.16.0 or later version on your local machine to run this code.

Install the dependencies and start the server.

```sh
$ cd cisco-front-end-test
$ npm install
$ npm start
```

### Tech

* [ReactJS]
* [styled-components]
* [ant.design]
* [Redux]
* [redux-thunk]
* [Fluid typography]

### Resources

* [Flat Icon] – to minimise size compared to importing all icons from Ant.design
* [Unsplash]
* [Google Fonts]

[ReactJS]: <https://reactjs.org/>
[styled-components]: <https://www.styled-components.com/>
[ant.design]: <https://ant.design/>
[Redux]: <https://redux.js.org/introduction/getting-started/>
[redux-thunk]: <https://github.com/reduxjs/redux-thunk/>
[Fluid typography]: <https://css-tricks.com/snippets/css/fluid-typography/>
[Flat Icon]: <https://www.flaticon.com/home>
[Unsplash]: <https://unsplash.com/>
[Google Fonts]: <https://fonts.google.com/>