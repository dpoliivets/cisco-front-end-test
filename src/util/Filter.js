const doFilter = (item, keyWord) => {
    if (!(keyWord instanceof RegExp)) {
        keyWord = new RegExp(keyWord, 'i')
    }

    return keyWord.test(item.name);
}

const createFilter = (keyWord) => {
    return (item) => doFilter(item, keyWord)
}

export { createFilter }
