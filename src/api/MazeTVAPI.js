// Import axios
import axios from 'axios'


/*
    Instantiate a default MazeTV API with base URL
*/
const mazeTVAPI = axios.create({
   baseURL: 'https://api.tvmaze.com/'
})

export default mazeTVAPI
