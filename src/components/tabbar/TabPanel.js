// Import dependencies
import React from 'react'

// Import components
import { Input, Empty } from 'antd'
import EpisodeCard from '../episode-card/EpisodeCard'
import { ContentLayout, ParagraphText } from '../../styled-components/UILibrary'

// Import styles
import './TabPanel.css'

// Import utilities
import { createFilter } from '../../util/Filter'

const { Search } = Input


/*
    Individual tab panel component
*/
export default class TabPanel extends React.Component {
   constructor(props) {
      super(props)
      this.state = {
         cleanData: this.props.data,
         data: this.props.data
      }
   }

   /*
        Filters episodes according to the
        search string (keyWord)
    */
   filterData(data, keyWord) {
      if (data && data.length > 0) {
         if (keyWord !== '') {
            return data.filter(createFilter(keyWord))
         }
      }
   }
   /*
      Triggers filter function
   */
   filterEpisode = (keyWord) => {
      if (keyWord === '') {
         this.setState({ data: this.state.cleanData })
         return
      }

      var result = this.filterData(this.state.cleanData, keyWord)
      this.setState({ data: result })
   }

   render() {
      // Get data
      const { openPopUp } = this.props

      return (
         <ContentLayout>
            <div className='tab-panel__top-panel'>
               <ParagraphText>Episodes</ParagraphText>

               <Search
                  placeholder='search episodes...'
                  onChange={(e) => this.filterEpisode(e.target.value)}
                  style={{ width: 200 }}
                  allowClear
                  size='large'
               />
            </div>

            {this.state.data.length === 0 ? (
               <div className='tab-panel__empty-container'>
                  <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
               </div>
            ) : (
               // Render fetched data
               this.state.data.map((data) => <EpisodeCard data={data} key={data.id} openPopUp={openPopUp} />)
            )}
         </ContentLayout>
      )
   }
}
