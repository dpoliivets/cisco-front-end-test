// Import dependencies
import React from 'react'
import ReactResizeDetector from 'react-resize-detector'

// Import components
import { Tabs, Spin } from 'antd'
import TabPanel from './TabPanel'
import PopUp from '../popup/PopUp'

// Import styles
import './TabBar.css'

// Import redux
import { connect } from 'react-redux'
import { getDoctorWhoData } from '../../redux/actions/DoctorWhoAction'

// Define tabs pane
const { TabPane } = Tabs


/*
    Component that holds all the tabs for each of the seasons.
    Calls a redux method that performs fetching.
*/
class TabBar extends React.Component {
   constructor(props) {
      super(props)
      this.state = {
         windowWidth: 0,
         popUpActive: false,
         popUpData: []
      }
      this.onResize = this.onResize.bind(this)
      this.openPopUp = this.openPopUp.bind(this)
      this.closePopUp = this.closePopUp.bind(this)
   }

   /*
      Callback on window resize
   */
   onResize = (width) => {
      this.setState({ windowWidth: width })
   }

   /*
      Opens pop-up for a currently selected episode
   */
   openPopUp = (data) => {
      this.setState({ popUpActive: true, popUpData: data })
   }

   /*
      Closes pop-up for a currently selected episode
   */
   closePopUp = () => {
      this.setState({ popUpActive: false })
   }

   /*
        Fetch Doctor Who data from MazeTV API
   */
   componentDidMount() {
      this.props.dispatch(getDoctorWhoData())
   }

   render() {
      // Get data
      const { data, isDataLoading } = this.props

      return (
         <ReactResizeDetector handleWidth onResize={this.onResize}>
            {isDataLoading ? (
               <div className='tabbar__loading-container' key='spin'>
                  <Spin size='large' />
               </div>
            ) : (
               <Tabs
                  defaultActiveKey='1'
                  tabPosition={this.state.windowWidth > 965 ? 'left' : 'top'}
                  size={this.state.windowWidth > 965 ? 'large' : 'default'}
                  key='tabs'
               >
                  {// Render fetched data
                  Object.keys(data).map((key) => (
                     <TabPane tab={'Season ' + key} key={key}>
                        <TabPanel data={data[key]} openPopUp={this.openPopUp} />
                     </TabPane>
                  ))}
               </Tabs>
            )}

            {/* Pop-up window with extended information about the rocket */}
            <PopUp active={this.state.popUpActive} closePopUp={this.closePopUp} data={this.state.popUpData} key='pop-up' />
         </ReactResizeDetector>
      )
   }
}

// Connect redux to component
const mapStateToProps = (state) => ({
   data: state.doctorWho.data,
   isDataLoading: state.doctorWho.isDataLoading,
   loadingFailed: state.doctorWho.loadingFailed
})
export default connect(mapStateToProps)(TabBar)
