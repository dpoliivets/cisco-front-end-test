// Import dependencies
import React from 'react'

// Import components
import { Spin } from 'antd'
import { ParagraphText, TitleText } from '../../styled-components/UILibrary'

// Import styles
import './EpisodeCard.css'


/*
    Episode Card component
*/
export default class EpisodeCard extends React.Component {
   constructor(props) {
      super(props)
      this.state = {
         imageLoaded: false
      }
      this.onImageLoaded = this.onImageLoaded.bind(this)
   }

   /*
      Callback when image is loaded
   */
   onImageLoaded = () => {
      this.setState({ imageLoaded: true })
   }

   render() {
      // De-structure
      const { data, openPopUp } = this.props

      return (
         <div className='ec__container' onClick={() => openPopUp(data)}>
            {/* Orange line on the left of component */}
            <div className='ec__indicator' />

            {/* Episode number */}
            <div className='ec__episode-container'>
               <TitleText>{data.number}</TitleText>
            </div>

            {/* Preview image */}
            <div className='ec__thumbnail-component'>
               <img src={data.image.medium} className='ec__thumbnail-image' onLoad={this.onImageLoaded} alt='episode-thumbnail' />
               {!this.state.imageLoaded ? (
                  <div className='ec__thumbnail-loader'>
                     <Spin size='large' />
                  </div>
               ) : null}
            </div>

            <div className='ec__info-container'>
               <TitleText>{data.name}</TitleText>
               <ParagraphText>{'Season ' + data.season}</ParagraphText>
            </div>
         </div>
      )
   }
}
