// Import dependencies
import React from 'react'

// Import components
import { Row, Col, Spin } from 'antd'
import { ParagraphText, TitleText } from '../../styled-components/UILibrary'

// Import styles
import './PopUp.css'

// Import images
import cross from '../../assets/svg/close.svg'


/*
    Pop-up panel component for displaying extra
    data about an episode
*/
export default class PopUp extends React.Component {
   constructor(props) {
      super(props)
      this.state = {
         isImageLoading: true
      }
   }

   /*
      Callback when image is loaded
   */
   onImageLoaded = () => {
      this.setState({ isImageLoading: false })
   }

   /*
      Callback when pop up is closed
   */
   onPopUpClose = () => {
      this.setState({ isImageLoading: true })
      this.props.closePopUp()
   }

   render() {
      // Get data
      const { data, active } = this.props

      return (
         <div className={active ? 'pop-up__container pop-up__active' : 'pop-up__container'}>
            {/* Close icon */}
            <div className='pop-up__close-icon' onClick={this.onPopUpClose}>
               <img src={cross} className='pop-up__cross' alt='cross' />
            </div>

            {/* Extra information */}
            {active ? (
               <Row type='flex' className='pop-up__info-container' gutter={60}>
                  {/* Description paragraph */}
                  <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                     <div className='pop-up__thumbnail-container'>
                        <img src={data.image.original} className='pop-up__thumbnail-image' onLoad={this.onImageLoaded} alt='episode-thumbnail' />
                        {this.state.isImageLoading ? (
                           <div className='pop-up__thumbnail-loader'>
                              <Spin size='large' />
                           </div>
                        ) : null}
                     </div>
                  </Col>
                  <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                     <TitleText>{data.name}</TitleText>
                     <ParagraphText margin='margin-top: 20px !important'>{data.summary.slice(3, data.summary.length - 4)}</ParagraphText>
                  </Col>
               </Row>
            ) : null}
         </div>
      )
   }
}
