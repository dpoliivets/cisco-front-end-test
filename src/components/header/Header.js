// Import dependencies
import React from 'react'

// Import components
import ScrollDown from './ScrollDown'
import { ParagraphText, HeaderTitleText } from '../../styled-components/UILibrary'

// Import styles
import './Header.css'

// Import images
import logo from '../../assets/svg/logo.svg'


/*
    Header component
*/
const Header = () => {
   return (
      <header className='header__container'>
         {/* Top Panel */}
         <div className='header__top-container'>
            <img src={logo} className='header-logo' alt='logo' />
            <div className='header__top-panel-info'>
               <ParagraphText colour={'#fff'} textAlign='right'>
                  by Dmitry Poliyivets
               </ParagraphText>
               <ParagraphText colour={'#DBDBDB'} textAlign='right' fontSize={'10px'}>
                  for Cisco
               </ParagraphText>
            </div>
         </div>

         {/* Title */}
         <div className='header__title'>
            <HeaderTitleText>
               Explore
               <br />
               Doctor Who
               <br />
               Episodes
            </HeaderTitleText>
         </div>

         {/* Scroll Down Component */}
         <ScrollDown />
      </header>
   )
}

export default Header
