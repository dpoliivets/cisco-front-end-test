// Import dependencies
import React from 'react'

// Import components
import DoctorWhoEpisodes from './layouts/DoctorWhoEpisodes'

// Import styles
import './App.css'


function App() {
   return <DoctorWhoEpisodes />
}

export default App
