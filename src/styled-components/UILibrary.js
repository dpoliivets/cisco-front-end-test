// Import dependencies
import styled from 'styled-components'

// Import constants
import { Constants } from '../constants/Constants'

/*
    Page layout components
*/
export const PageContainer = styled.div`
    width: 100vw;
    height: auto;
    background-image: linear-gradient(45deg, #3C4550 0%, #272D35 100%);
`;
export const ContentLayout = styled.main`
    width: 100%;
    padding-left: 50px;
    padding-right: 8%;
    margin-top: 100px;

    @media (max-width: 965px) {
        padding-left: 8%;
        margin-top: 50px;
    }
`;

/*
    Fluid text components
*/
export const HeaderTitleText = styled.h1`
    font-family: 'Orbitron', sans-serif;
    font-weight: 700;
    font-size: calc(${ Constants.minHeaderTitle }px + (${ Constants.maxHeaderTitle } - ${ Constants.minHeaderTitle }) * ((100vw - 300px) / (1600 - 300)));
    line-height: 1.5;
    letter-spacing: 6px;
    color: ${ Constants.headerFontColour };
    margin: 0 !important;
    text-shadow: 3px 3px 8px #000000;
`;
export const ParagraphText = styled.p`
    font-family: 'Orbitron', sans-serif;
    font-weight: 400;
    font-size: calc(${ Constants.minParagraphText }px + (${ Constants.maxParagraphText } - ${ Constants.minParagraphText }) * ((100vw - 300px) / (1600 - 300)));
    letter-spacing: 2px;
    line-height: 1.7;
    color: ${ Constants.fontColour };
    margin: 0 !important;
    ${ props => props.colour ? 'color: '+props.colour : ''};
    ${ props => props.textAlign ? 'text-align: '+props.textAlign : ''};
    ${ props => props.fontSize ? 'font-size: '+props.fontSize : ''};
    ${ props => props.margin ? props.margin : ''};
`;
export const TitleText = styled.h1`
    font-family: 'Orbitron', sans-serif;
    font-weight: 700;
    font-size: calc(${ Constants.minTitle }px + (${ Constants.maxTitle } - ${ Constants.minTitle }) * ((100vw - 300px) / (1600 - 300)));
    line-height: 1.5;
    letter-spacing: 3px;
    color: ${ Constants.titleColour };
    margin: 0 !important;
`;
