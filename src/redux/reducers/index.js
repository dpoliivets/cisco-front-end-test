import { combineReducers } from 'redux'
import doctorWho from './DoctorWhoReducer'

export default combineReducers({
   doctorWho
})
