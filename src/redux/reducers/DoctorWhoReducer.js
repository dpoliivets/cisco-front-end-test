/*
    Import action types for Doctor Who data fetching
*/
import { FETCH_DOCTOR_WHO_BEGIN, FETCH_DOCTOR_WHO_SUCCESS, FETCH_DOCTOR_WHO_FAILURE } from '../actions/DoctorWhoAction'

/*
    Define default state
*/
let defaultState = {
   data: [],
   isDataLoading: true,
   loadingFailed: false
}

/*
    Define Doctor Who reducer
*/
const doctorWhoReducer = (state = defaultState, action) => {
   switch (action.type) {
      case FETCH_DOCTOR_WHO_BEGIN:
         return {
            isDataLoading: true
         }
      case FETCH_DOCTOR_WHO_SUCCESS:
         return {
            isDataLoading: false,
            data: action.payload
         }
      case FETCH_DOCTOR_WHO_FAILURE:
         return {
            isDataLoading: true,
            loadingFailed: true
         }
      default:
         return state
   }
}

export default doctorWhoReducer
