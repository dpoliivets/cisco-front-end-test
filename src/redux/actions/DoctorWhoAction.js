// Import API
import mazeTVAPI from '../../api/MazeTVAPI'

/*
    Define action types for Doctor Who data fetching
*/
export const FETCH_DOCTOR_WHO_BEGIN = 'FETCH_DOCTOR_WHO_BEGIN'
export const FETCH_DOCTOR_WHO_SUCCESS = 'FETCH_DOCTOR_WHO_SUCCESS'
export const FETCH_DOCTOR_WHO_FAILURE = 'FETCH_DOCTOR_WHO_FAILURE'


/*
    Action for pulling data from MazeTV API
*/
export function getDoctorWhoData() {
   return (dispatch) => {
      // Dipatch begin event
      dispatch(fetchDoctorWhoBegin())
      mazeTVAPI
         .get('/singlesearch/shows?q=doctor%20who&embed=episodes')
         .then((response) => {
            // Prepare data
            let episodes = response.data._embedded.episodes
            let result = {}

            for (let i = 0; i < episodes.length; i++) {
               if (result[episodes[i].season]) result[episodes[i].season].push(episodes[i])
               else result[episodes[i].season] = [episodes[i]]
            }

            dispatch(fetchDoctorWhoSuccess(result))
         })
         .catch((error) => {
            dispatch(fetchDoctorWhoFailure())
         })
   }
}

export const fetchDoctorWhoBegin = () => ({
   type: FETCH_DOCTOR_WHO_BEGIN
})
export const fetchDoctorWhoSuccess = (data) => ({
   type: FETCH_DOCTOR_WHO_SUCCESS,
   payload: data
})
export const fetchDoctorWhoFailure = () => ({
   type: FETCH_DOCTOR_WHO_FAILURE
})
