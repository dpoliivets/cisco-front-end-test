// Import dependencies
import React from 'react'

// Import components
import Header from '../components/header/Header'
import TabBar from '../components/tabbar/TabBar'
import Footer from '../components/footer/Footer'
import { PageContainer } from '../styled-components/UILibrary'


/*
   Main Layout Component
*/
const DoctorWhoEpisodes = () => {
   return (
      <PageContainer>
         {/* Header */}
         <Header />

         {/* Page Content */}
         <TabBar />

         {/* Footer */}
         <Footer />
      </PageContainer>
   )
}

export default DoctorWhoEpisodes
