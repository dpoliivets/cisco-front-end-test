/*
    This file contains constants used
    throughout the web app
*/
export const Constants = {
    // Font Colours
    fontColour: '#8A919A',
    headerFontColour: '#FFFFFF',
    titleColour: '#FFFFFF',
    // Font Sizes
    minParagraphText: 12,
    maxParagraphText: 14,
    minHeaderTitle: 30,
    maxHeaderTitle: 56,
    minTitle: 16,
    maxTitle: 22,
};